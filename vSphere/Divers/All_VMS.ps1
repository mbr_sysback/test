<#
    .SYNOPSIS
      Generiert eine Liste von VMs mit verschieden Werten in dem über $Filelocation definierten Pfad
      .Description
        Ermöglicht das erstellen einer Liste mit VMs die verschiedene Werte enthält.
        Entsprechend der benutzten Filter (DataCenter, Cluster oder Folder) und vCenter
        wird der Filename in $Filelocation generiert.

        Folgende Werte sind Möglich:
        "vmName";"vmDatacenter";"vmCluster";"vmHost";"vmHostModel";"vmHostVersion";"vmHostCPUModell";
        "vmMemory";"vmCores";"vmGuestBS";"vmGuestBSFull";"vmPowerstate";"vmDatastores";"vmPlatzBedarf";
        "vmIPs";"vmVLANs";"vmMandAssyst";"vmCI-Status";"vmAnsprechPartner";"vmAnwendung"

        Eine Abwahl nicht benötigter Werte muss in der ScriptDatei durch # erfolgen.

      .Parameter vCenter
      vCenter welches für die Abfragen benutzt werden soll, falls kein vCenter angegeben wird ist der default
      XXXXXXXXXX
      .Parameter DataCenter
      Datacenter in welchem die VMs gesucht werden.
      .Parameter Cluster
      Cluster in welchem die VMs gesucht werden.
      .Parameter Folder
      Folder in welchem die VMs gesucht werden.
      .Parameter ALL
      Sicherheitsswitch falls kein DataCenter, Cluster oder Folder angegeben wird, damit alles VMs gesucht werden können.
    .Example
     VM-Alle-Erfassen -Datacenter DC
      Erzeugt eine Liste von allen VM's im Datacenter DC
       .Notes
       12.05.14 Initiale Erstellung
#>


#[CmdletBinding()]
Param(
      [string]$vCenter = 'XXXXXXXXXX',
      [Parameter(ParameterSetName='DataCenter', Mandatory=$true)]
      [string]$DataCenter = '',
      [Parameter(ParameterSetName='Cluster', Mandatory=$true)]
      [string]$Cluster = '',
      [Parameter(ParameterSetName='Folder', Mandatory=$true)]
      [string]$Folder = '',
      [Parameter(ParameterSetName='ALL', Mandatory=$true)]
      [switch]
      $ALL
)

Function Main {

    $Filelocation = 'd:\ablage'

    if (-not (Get-PSSnapin VMware.VimAutomation.Core -ErrorAction SilentlyContinue)) {
          Add-PSSnapin VMware.VimAutomation.Core
    }




    $Filter = ''
    Connect-VIServer $vCenter
    [array]$report =@()

    # Setzen von Filter und Searchroot
    if ($Folder -ne '' -and $Folder -ne $null)
          {
          $Filter = "Folder_$Folder"
          $SearchRoot =Get-Folder $Folder
          }
    elseif ($Cluster -ne '' -and $Cluster -ne $null)
          {
          $Filter = "Cluster_$Cluster"
          $SearchRoot =Get-Cluster $Cluster
          }
    elseif ($DataCenter -ne '' -and $Datacenter -ne $null)
          {
          $Filter = "DC_$DataCenter"
          $SearchRoot =Get-Datacenter $DataCenter
          }

    if ($Filter -ne '')
          {
          $vms = Get-View -ViewType VirtualMachine -SearchRoot $SearchRoot.id
          }
    else
          {
          Write-Host 'Keine Filter Funktion angegeben. Setze Filter auf ALL!'
          $Filter = 'ALL'
          $vms = Get-View -ViewType VirtualMachine
          }






    foreach ($vmview in $vms) {

          Write-Host $vmview.Name
          $VMObject = Get-VIObjectByVIView $vmview
          $vm = New-Object PsObject


          # Durch # vor den verschiedenen Zeilen einfach die nicht gewünschten Werte deaktivieren!
          # Wenn immer möglich $vmview benutzen!
          #
          # Die möglichen Werte bekommt man über $vmview = Get-View -ViewType VirtualMachine -Filter @{"Name" = "VM"}
          #
          # Neue Werte können über $vm = addValue $VM 'Wertname' Wert oder über
          # Add-Member -InputObject $vm -MemberType NoteProperty -Name WertName -Value Wert
          # hinzugefügt werden.

          $vm = addValue $vm vmName $vmview.name
          $vm = addValue $vm vmDatacenter $VMObject.vmHost.Parent.ParentFolder.Parent
          $vm = addValue $vm vmCluster $VMObject.vmHost.Parent
          $vm = addValue $vm vmHost $VMObject.vmhost.name
          $vm = addValue $vm vmHostModel $VMObject.vmhost.model
          $vm = addValue $vm vmHostVersion $VMObject.vmhost.apiversion
          $vm = addValue $vm vmHostCPUModell $VMObject.vmhost.ProcessorType
          $vm = addValue $vm vmMemory $vmview.Config.Hardware.MemoryMB
          $vm = addValue $vm vmCores ($vmview.Config.Hardware.NumCoresPerSocket * $vmview.Config.Hardware.NumCPU)
          $vm = addValue $vm vmGuestBS $vmview.Config.GuestId
          $vm = addValue $vm vmGuestBSFull $vmview.guest.GuestFullName
          $vm = addValue $vm vmPowerstate $vmview.Guest.GuestState
          $vm = addValue $vm vmDatastores (($vmview.Config.DatastoreUrl | ForEach-Object {$_.Value}) -join ",")
          $vm = addValue $vm vmPlatzBedarf ([math]::round((Get-VmSize $vmview) /1Gb, 3))
          $vm = addValue $vm vmIPs (($vmview.Guest.IpAddress  | Where-Object {$_.split(".").length -eq 4}) -join ",")
          $vm = addValue $vm vmVLANs (($vmview.Guest.Net | ForEach-Object {$_.Network}) -join ",")
          $vm = addValue $vm vmAnsprechPartner (Get-CustomAnnotation $vmview 'Ansprechpartner')
          $vm = addValue $vm vmAnwendung (Get-CustomAnnotation $vmview  'Anwendung')


          Write-Host $vm
          $report +=$vm
    }
    Write-Host "Done!"


    $report |Export-Csv "$Filelocation\AllVMS-$vCenter-$Filter-$(Get-Date -format yyyyMMddHHmmss).csv" -NoTypeInformation -UseCulture



}

###########################################################################################################

# Functions


# Von http://www.van-lieshout.com/2009/07/how-big-is-my-vm/
function Get-VmSize($vm)
{
    #Initialize variables
    $VmDirs =@()
    $VmSize = 0

    $searchSpec = New-Object VMware.Vim.HostDatastoreBrowserSearchSpec
    $searchSpec.details = New-Object VMware.Vim.FileQueryFlags
    $searchSpec.details.fileSize = $TRUE


        #Create an array with the vm's directories
        $VmDirs += $vm.Config.Files.VmPathName.split("/")[0]
        $VmDirs += $vm.Config.Files.SnapshotDirectory.split("/")[0]
        $VmDirs += $vm.Config.Files.SuspendDirectory.split("/")[0]
        $VmDirs += $vm.Config.Files.LogDirectory.split("/")[0]
        #Add directories of the vm's virtual disk files
        foreach ($disk in $vm.Layout.Disk) {
            foreach ($diskfile in $disk.diskfile){
                $VmDirs += $diskfile.split("/")[0]
            }
        }
        #Only take unique array items
        $VmDirs = $VmDirs | Sort | Get-Unique

        foreach ($dir in $VmDirs){
            $ds = Get-Datastore ($dir.split("[")[1]).split("]")[0]
            $dsb = Get-View (($ds | get-view).Browser)
            $taskMoRef  = $dsb.SearchDatastoreSubFolders_Task($dir,$searchSpec)
            $task = Get-View $taskMoRef

            while($task.Info.State -eq "running" -or $task.Info.State -eq "queued"){$task = Get-View $taskMoRef }
            foreach ($result in $task.Info.Result){
                foreach ($file in $result.File){
                    $VmSize += $file.FileSize
                }
            }
        }

    return $VmSize
}

# Wie bekommant man die benötigten Annonation Werte heraus?
# Eine VM auswählen via $vm = Get-View -ViewType VirtualMachine -Filter @{'name'='Y070SPUMK1PI029'}
# Dann kann man über $vm.AvailableField sich alle Felder anzeigen lassen und über
# $vm.Summary.CustomValue sich die entsprechenden Felder anzeigen lassen.
function Get-CustomAnnotation($vm, $annotationName) {
      $customAnnotation = ''
      $annotationKey = ($vm.Availablefield | ? {$_.Name -eq "$annotationName"}).key
      $customAnnotation = ($vm.Summary.CustomValue | ? {$_.Key -eq $annotationKey}).value
      return $customAnnotation
}

# http://stackoverflow.com/a/22362868
Function Pause {
      Param([Parameter(ValueFromRemainingArguments=$true)][string]$Message = “Press any key to continue . . . ” )
      # Funktioniert nur in Powershell > v3
    if ((Test-Path variable:psISE) -and $psISE) {
        $Shell = New-Object -ComObject "WScript.Shell"
        $Button = $Shell.Popup("Click OK to continue.", 0, "Script Paused", 0)
    }
    else {
        Write-Host -NoNewline $Message
        [void][System.Console]::ReadKey($true)
        Write-Host
    }
}

Function addValue {
      Param(
            [Parameter(Mandatory=$True,Position=1)]
            [PSObject] $vm,
            [Parameter(Mandatory=$True,Position=2)]
            [string] $name,
            [Parameter(Position=3)]
            $value
            )
      if ($value -eq $null) {$value = ''}
      Add-Member -InputObject $vm -MemberType NoteProperty -Name $name -Value $value
      return $vm
}

$startDTM = (Get-Date)
main
$endDTM = (Get-Date)

# Echo Time elapsed
Write-Host "Elapsed Time: $(($endDTM-$startDTM).totalseconds) seconds"
Pause

