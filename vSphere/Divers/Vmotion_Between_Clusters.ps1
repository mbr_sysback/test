#Author: Sebastian Barylo
#URL: http://vmwaremine.com/2014/02/02/powercli-vmotion-vm-between-clusters/#sthash.uXhLLoei.dpbs
#general scope variables, customize migration targets (vhost, datastore, portgroup)if needed
$batch_input = "<path to input file>"
$vCenter = 'vc01.lab.local'
$target_vhost = Get-vmhost -name esx02.lab.local -server $vCenter
$target_datastore = get-datastore -name DS01 -server $vCenter
$target_pg = get-vdswitch -name DV01 -server $vCenter | get-vdportgroup -name 'PG-01' -server $vCenter
#load names of vms to be processed from file
$vm_names = Get-Content $batch_input
#for each name, retrieve vm object and process it
foreach ($vm_name in $vm_names) {
	$vm = get-vm -name $vm_name -server $vCenter
	#reconfigure vnic to standard vswitch to enable migration
	get-networkadapter -vm $vm -name "Network adapter 1" -server $vCenter |
	set-networkadapter -networkname 'VM Network' -connected:$false -confirm:$false|
	Out-Null
	#(s)vmotion the vm
	move-vm -vm $vm -destination $target_vhost -datastore $target_datastore -server $vCenter |
	Out-Null
	#retrieve the vm object again and connect it to target vdswitch, set-networkadapter is invoked twice
	#because it doesn't seem to allow connecting to vdswitch and changing link state to "connected" in one go (bug?)
	get-vm -name $vm_name -server $vCenter |
	get-networkadapter -name "Network adapter 1"|
	set-networkadapter -connected:$true -confirm:$false|
	set-networkadapter -portgroup $target_pg -confirm:$false|
	Out-Null
	Write-Host "$vm_name --!!!DONE!!!---"
}