$vc = "vcserver"
# The coming line assuming that you have already saved your vCenter Credentials into Credentials.xml
# $cred = Get-VICredentialStoreItem -Host $vc -File c:\scripts\Credentials.xml
# Connect to your vCenter
# $vCenter = Connect-VIServer $vc -user $cred.user -Password $cred.password
$vCenter = Connect-VIServer $vc 
# Get full list of all ESXi Hosts available under your vCenter server
$HostsList = Get-VMHost
# Looping through the whole Hosts list
foreach ($Hosts in $HostsList)
{
	$VMHost = $Hosts.Name
	Write-Host Modifying the Multipathing policy on host $VMHost
	# Connect to esxcli commands environment on the specific Host
	$esxcli = Get-EsxCli -VMHost $VMHost
	# Get a list of all available LUNs, sorting them by device name
	# $lunList = Get-VMHost $VMHost | Get-ScsiLun -CanonicalName �*� | % {$_.CanonicalName}
	$lunList = Get-VMHost $VMHost | Get-ScsiLun -CanonicalName �naa.*� | % {$_.CanonicalName}
	# Looping through all available LUNs
	foreach ($lun in $lunList)
		{
		write-Host Executing set policy RR per device on $lun
		# Perform the required Multipathing Policy change to Round Robin against the specific LUN
		$esxcli.storage.nmp.device.set($null,$lun,�VMW_PSP_RR�)
	}
}
# Disconnect from all Hosts & vCenters
Disconnect-VIServer *