$vc = �server�
# The coming line assuming that you have already saved your vCenter Credentials into Credentials.xml
# $cred = Get-VICredentialStoreItem -Host $vc -File c:\scripts\Credentials.xml
# Connect to your vCenter
# $vCenter = Connect-VIServer $vc -user $cred.user -Password $cred.password
$vCenter = Connect-VIServer $vc -user administrator -Password pw
# Get full list of all ESXi Hosts available under your vCenter server
$HostsList = Get-VMHost
# Looping through the whole Hosts list
foreach ($Hosts in $HostsList)
{
	$VMHost = $Hosts.Name
	Write-Host Disabling VAAI on host $VMHost
	Set-VMHostAdvancedConfiguration -VMHost $VMHost -Name DataMover.HardwareAcceleratedMove -Value 0
	Set-VMHostAdvancedConfiguration -VMHost $VMHost -Name DataMover.HardwareAcceleratedInit -Value 0
    Set-VMHostAdvancedConfiguration -VMHost $VMHost -Name VMFS3.HardwareAcceleratedLocking -Value 0
}
# Disconnect from all Hosts & vCenters
Disconnect-VIServer *