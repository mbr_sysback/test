<#
.SYNOPSIS
   Triggers Shutdown or vMotion of VM in a specific Datacenter or all Datacenters
.DESCRIPTION
   Takes a Datacenter (or ALL) and an evacuation action (shutdown or vmotion) to evacuate the
   VM's from the Datacenter
.PARAMETER rz
   the Datacenter which is being evacuated, either rz1, rz2 or all
.PARAMETER evaOption
   the evacuation option, either shutdown, vmotion or dryrun
.PARAMETER help
   displays the full help
#>

param (
	[parameter(HelpMessage="[rz1|rz2|all]")]
	[ValidateSet("rz1","rz2","all")]
	[string]
	$rz ="all",
	[parameter(HelpMessage="[vmotion|shutdown|dryrun]")]
	[ValidateSet("vmotion","shutdown","dryrun")]
	[string]$EvaOption ="dryrun",
	[switch]$help
)
Add-PSSnapin VMware.VimAutomation.Core

Write-Host "Initializing Script"
Start-Transcript -Path C:\ups\shutdown_$rz.log -Append
$ScriptName = $MyInvocation.MyCommand.Name

# Werte f�r die Scriptausf�hrung

# Wartezeit bis davon ausgegangen wird das die gerade ausgef�hrte Aktion h�ngt
$script:waittime = 200 #Seconds
# vCenter Server Informationen
$script:vCenter = "vcenter"
$script:vCenterUser = "username"
$script:vCenterPW = "password"
$script:vCenterName = "vcentername"
$script:powerCliVMName = "powercliVM"
$script:rz1Server = @("10.1.33.201")
$script:rz2Server = @("10.1.33.202")
$script:numVMs = 0
$script:targetVM = @{}
$script:targetSrv = @()

# Ausgabe der Syntax
function usage {
	Write-Host ""
	Write-Host "Syntax: $ScriptName  -RZ <RZ> -EvaOption <EvacuateOption>"
	Write-Host ""
	Write-Host "<RZ> 			= welches RZ einen Stromausfall hat		[rz1|rz2|all]"
	Write-Host "<EvacuateOption>	= wie das RZ evakuiert werden soll		[vmotion|shutdown|dryrun]"
	Write-Host ""
	Write-Host "Falls das RZ weggelassen wird, gilt als default ALL und"
	Write-Host "es muss als EvacuateOption shutdown oder dryrun gew�hlt sein!"
	Write-Host ""
	Write-Host "Falls keine EvacuateOption genannt wird, ist der default dryrun"
	Write-Host ""
	endScript
}

# �berpr�fen ob die Syntax eingehalten wird
function checkSyntax {

	if ($rz -eq "all") {
		if (($evaOption -ne "shutdown") -and ($evaOption -ne "dryrun")) {
			usage
		}
	}
}

# Erstellen der TargetHost Liste
function createTargetHosts {
	if ($rz -eq "rz1") {
		$script:rzServer = $rz1Server
		$script:evaServer = $rz2Server
	}
	elseif ($rz -eq "rz2") {
		$script:rzServer = $rz2Server
		$script:evaServer = $rz1Server
	}
	elseif ($rz -eq "all") {
		$script:rzServer = $rz1Server + $rz2Server
		$script:evaServer = "Keiner"
	}
	else {
		usage
	}
}

# Count Shutdown VM's
function countShutdownVMs {
	$vcounter=0
	foreach ($VM in ($sourceSrv | Get-VM | where {($_.PowerState -eq "PoweredOn") -and (($_.Name -ne $vCenterName) -and ($_.Name -ne $powerCliVMName))})){
		$vcounter++
	}
	$script:numVMs = $vcounter
}

# Shutdown der VM
function shutdown {
	
	foreach ($VM in ($sourceSrv | Get-VM | where {($_.PowerState -eq "PoweredOn") -and (($_.Name -ne $vCenterName) -and ($_.Name -ne $powerCliVMName))})){
		Write-Host "Shutting down $vm"
        $totalVMs++
		Shutdown-VMGuest -VM $vm -Confirm:$false 
	}
	$script:numVMs = 1
	$Newtime = 0 # Seconds
	$Time = (Get-Date).TimeofDay
	do {
		# Wait for the VMs to be Shutdown  cleanly / vmotioned off
		sleep 1.0
		$timeleft = $waittime - $Newtime
		countShutdownVMs
		$vmsleft = $totalVMs - $script:numVMs
		Write-Progress -Activity "Warte auf Shutdown VMS oder noch $timeleft Sekunden" -Status "$vmsleft von $totalVMs VMs beendet" -PercentComplete (($vmsleft/ $totalVMs ) * 100)
		$Newtime = (Get-Date).TimeofDay - $Time
		$Newtime = $Newtime.Minutes * 60 + $Newtime.Seconds
	} until (($script:numVMs -eq 0) -or ($Newtime -ge $waittime))
	
	if ($script:rz -ne "all") {
		vmotion
	}

}

# vMotion der VM
function vmotion {
	foreach ($esx in $evaServer) {
		$tempESX = Get-VMHost $esx
		if ($tempESX) {
			$targetSrv += $tempESX | ?{$_.ConnectionState -eq "Connected"}
		}
	}
	foreach ($VM in ($sourceSrv | Get-VM | where {$_.PowerState -eq "poweredOn"})){
		$targetHost = Get-Random $targetSrv
		Write-Host "vMotion $vm nach $targetHost"
		$task = Move-VM -VM $Vm -Destination $targetHost -RunAsync
		$targetVM[$vm] = $task 
	}
	$script:numVMs = 1
	$Newtime = 0 # Seconds
	$Time = (Get-Date).TimeofDay
	$totalVMs = $targetVM.Count
	do {
		# Wait for the VMs to be vmotioned off
		sleep 1.0
		$timeleft = $waittime - $Newtime
		countVmotionVMS
		$vmsleft = $totalVMs - $script:numVMs
		Write-Progress -Activity "Warte auf vMotion der VMS oder noch $timeleft Sekunden" -Status "$vmsleft von $totalVMs VMs beendet" -PercentComplete (($vmsleft/ $totalVMs ) * 100)
		$Newtime = (Get-Date).TimeofDay - $Time
		$Newtime = $Newtime.Minutes * 60 + $Newtime.Seconds
	} until (($script:numVMs -eq 0) -or ($Newtime -ge $waittime))
	sleep 2.0
}

# DryRun der VM... dh. nur logging der gefundenen VM's
function dryrun {
	foreach ($esx in $evaServer) {
		$tempESX = Get-VMHost $esx
		if ($tempESX) {
			$targetSrv += $tempESX | ?{$_.ConnectionState -eq "Connected"}
		}
	}
	foreach ($VM in ($sourceSrv | Get-VM | where {$_.PowerState -eq "poweredOn"})){
		$targetHost = Get-Random $targetSrv
		Write-Host "vMotion $vm nach $targetHost oder"
		Write-Host "Shutdown $vm"
	}
}

# Check falls Stromausfall in beiden RZ
function blackout {
	if ($evaOption -eq "dryrun") {
		Write-Host "Dryrun Simulation"
		if (Test-Path c:\ups\rz1) {Write-Host "RZ1 Shutdown l�uft bereits"}
		if (Test-Path c:\ups\rz2) {Write-Host "RZ2 Shutdown l�uft bereits"}
		if (Test-Path c:\ups\all) {Write-Host "ALL Shutdown l�uft bereits"}
	}
	elseif (Test-Path c:\ups\all) {
		Write-Host "Shutdown ALL ist bereits gestartet, tue nichts"
		endScript
	}
	else {
		switch ($rz) {
			"rz1" { 
				Set-Content c:\ups\rz1 ""
				if (Test-Path c:\ups\rz2) {
						Write-Host "Ein anderes UPS Script l�uft bereits, alle VM's werden ausgeschaltet!"
						$rz = "all"
						$script:rzServer = $rz1Server + $rz2Server
						$script:evaOption = "shutdown"
					}
		
				}
			"rz2" {
				Set-Content c:\ups\rz2 ""
				if (Test-Path c:\ups\rz1) {
						Write-Host "Ein anderes UPS Script l�uft bereits, alle VM's werden ausgeschaltet!"
						$rz = "all"
						$script:rzServer = $rz1Server + $rz2Server
						$script:evaOption = "shutdown"
					}
				}
			"all" {
				Set-Content c:\ups\all ""
			}
		}
	}
}

# Count number of VMs in Tasks
function countVmotionVMs {
	$tempnum = 0
	$targetVM.GetEnumerator() | Foreach-Object {
		if ($_.Value.State -eq "Running") {
			$tempnum++
			}
		}
	$script:numVMs = $tempnum
}

# Shutdown Hosts
function shutdownHosts {
	Write-Host "Shutting down Hosts"
	foreach ($esx in $sourceSrv) {
		Write-Host $esx
		$esxtarget = Get-View $esx 
		$esxtarget.ShutdownHost_Task($true)
		}
	Write-Host "vSphere Shutdown Complete"
}

# End the script
function endScript {
	Stop-Transcript
	exit
}

########################## Start HauptScript ###############################
if($help) { usage }

Write-Host "Checking Syntax"
checkSyntax
Write-Host "Creating Target Hosts"
createTargetHosts
Write-Host "Checking for Blackout"
blackout
Write-Host ""
Write-Host "$evaOption VMs aus RZ $rz nach $evaServer"
Write-Host ""
Write-Host "Loging into vCenter"
Connect-VIServer $vCenter -User $vCenterUser -Password $vCenterPW

# Get All the ESX Hosts

$sourceSrv = @()
foreach ($esx in $rzServer) {
	$tempESX = Get-VMHost $esx
	if ($tempESX) {
		$sourceSrv += $tempESX | ?{$_.ConnectionState -eq "Connected"}
	}
}

switch ($evaOption)
	{
		"vmotion" {vmotion}
		"shutdown" {shutdown}
		"dryrun" {dryrun}
		"default" {dryrun}
	}

switch ($rz) {

	"rz1" { Remove-Item c:\ups\rz1 }
	"rz2" { Remove-Item c:\ups\rz2 }
	"all" { Remove-Item c:\ups\all }
}

if ($evaOption -ne "dryrun") { 
	shutdownHosts 
}
endScript


	

